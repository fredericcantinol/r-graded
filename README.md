# General considerations

- The project must be carried out by group: 2 to 3 students max
- Project duration : 3 weeks
- Rendering :
    - a pdf or docx file containing:
    - the description of the project
    - Script, Results and Comments
- Your rendering must be uploaded on canvas as a Zip File : ID-Name-Firstname-campus.
- The quality of writing and the comments an interpretation of results will be criteria to be taken into account during the notation.
**Grading Scale : 150 pt**
  
Part 1 : 30 pt

Part 2 : 30 pt

Bonus : 10 pt
Part 3 : 40 pt
Writing & comments: 10 pt
Presentation & demo : 30 pt

 

 

# Objectives of the project :

- Analyze a data set,
- Create the association rules
- Create a recommendation engine

## The project is divided into 3 successive and complementary parts :

## Project Part 1 :

**In this part you will analyze data in preparation for association-rule mining with R. This part allows you to analyze the transaction data by identifying basic patterns in the data set. The goal is to show you typical analysis on a transaction data set : number of transaction, number of items, number of items/transaction, frequencies of item, transaction, etc.**


Overview
For this part, you will work on a data set  _Grocery_ that comes with the arules package. You will:
- Explore the data set and inspect it for basic patterns

 

Step 1 : Data Loading and Inspection

Download the  _arules_ package from CRAN site and load it  in RStudio


Step2 : Load the Groceries data set that comes with the _arules_  package:

Step 3 : Determine which R function allows you to familiarize yourself with the data in the data set, in order to answer the following questions:
_a. How many items are in the data set?_

_b. How many transactions are in the data set?_

_c. What is the density value?_

_d. What is meant by this value?_

_e. Which are the most commonly found items in the data set?_

_f. What percentage of transactions contain yoghurt?_

_g. How many transactions have only seven items?_

_h. How many transactions have two items?_

_i. What is the average number of items in a transaction?_

 

**_Bonus : you can add your own analysis_**

 
Step 4. Use the R function to view transactions 3 through 6.

Step 5. Use the R function to see the proportion of transactions that contain the first item:

Step 6. Use the R function to view the proportion of transactions that contain of a number of other items

Step 7.Use the R function to visually display the proportion of transactions containing items that have at least 15% support

Step 8. Display the proportions of the top 10 items:

Step 9. Visualize the entire sparse matrix, including all the items of the data set

 

 

## Project Part 2 :

## Objectives
**In this part, you will perform association-rule mining with R. This part of the project allows you to analyze the transaction data by identifying frequently co-occurring items in the data set. The goal is to show you how association-rules mining in R can be used to draw relationships between seemingly unrelated items.**


## Overview

## You will work on the same data set used in the previous part. You will

- Use the _Apriori_ algorithm on the data set to mine association rules ( this library in available in R)
- Evaluate the derived rules through their measures of **support, confidence, and lift**

**Step 1: Data loading and text preprocessing**
Find the association items in the Grocesseries data set using the default support and confidence
 

_You need to see  the definition of  Support, confidence and lift_

**Step 2 :** Try different support and confidence levels to generate different numbers of rules

**Step 3 :**  Use the R function to get a high level overview of the generated rules and to answer the following questions :

_a. How many rules have three items (lrs and rhs)?_

_b. Take a closer look at the first ten rules._

_c. Are these rules interesting? What three categories are used to define "interesting"?_

**Step 4 :** Take a closer look at the ten best rules.
Hint : Use lift as a measure of "best."

**Step 5 :**  Find any rules that contain the word "chocolate" and store it in an object called chocrules


**Step 6 :** View the rules

 

 

## Project Part 3 :

## Objectives
**In this part, you will build a recommender system from transaction data with R.**

**This part allows you to make recommendations from transaction data by using co-occurring items in the data set.**

**The goal is to show how to build recommenders.**


## Overview
**You will work on the same Groceries data set.**

You will have to :
- Identify unique users and items from the data set
- Use frequently co-occurring items to make recommendations to users about items that frequently co-exist with items they have given high ratings to

You need to use the package : _plyr_

_Hints :_

Create a function to calculate a list of unique users.

Create a function to calculate a list of unique items.

Establish Product List Index.

Create a recommendation function

Generate recommendations for each of the users